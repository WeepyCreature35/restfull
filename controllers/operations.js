const express=require('express');
//Modelo=Usuario={email,nombre,apellido,pass}
var n1=0;
var n2=0;
var n3=0;
function suma(req,res,next){
    n1=req.params.n1;
    n2=req.params.n2;
    n3=Number(n1)+Number(n2);
    res.send(`La suma es: ${n3}`);
}
function multi(req,res,next){
    n3=n1*n2;
    res.send(`La multiplicacion es: ${n3}`);
}
function div(req,res,next){
    n3=n1/n2;
    res.send(`La division es: ${n3}`);
}
function potencia(req,res,next){
    n3=Math.pow(n1,n2);
    res.send(`La potencia es: ${n3}`);
}
function resta(req,res,next){
    n1=req.params.n1;
    n2=req.params.n2;
    n3=n1-n2;
    res.send(`La resta es: ${n3}`);
}
module.exports={
    suma, multi, div, potencia, resta
}